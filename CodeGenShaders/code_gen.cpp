#include "pch.h"
#include "code_gen.h"

const unordered_map<string, string> basic_type_map(
	{
		{ "bool", "bool" },
		{ "int", "int" },
		{ "float", "float" },
		{ "vec2", "glm::vec2" },
		{ "vec3", "glm::vec3" },
		{ "vec4", "glm::vec4" },
		{ "mat2", "glm::mat2" },
		{ "mat3", "glm::mat3" },
		{ "mat4", "glm::mat4" },
		{ "sampler2D", "int" },
		{ "samplerCube", "int" }
	});

const unordered_map<string, string> basic_type_to_call_map(
	{
		{ "bool", "SetInt" },
		{ "int", "SetInt" },
		{ "float", "SetFloat" },
		{ "vec2", "SetVec2" },
		{ "vec3", "SetVec3" },
		{ "vec4", "SetVec4" },
		{ "mat2", "SetMat2" },
		{ "mat3", "SetMat3" },
		{ "mat4", "SetMat4" },
		{ "sampler2D", "SetInt" },
		{ "samplerCube", "SetInt" }
	});

string GenTab(size_t count)
{
	string res;

	for (size_t i = 0; i < count; i++)
	{
		res += "\t";
	}

	return res;
}

string GenBeginFile()
{
	string res;

	res += "#pragma once\n";
	res += "#include \"shader_base.h\"\n";

	return res;
}

string GenClassBegin(const ProgramInfo& info)
{
	string res;

	res += "struct " + info.name + " final : public ShaderBase\n";
	res += "{\n";

	return res;
}

string GenClassConstructor(const ProgramInfo& info, const string& src_vert, const string& src_frag, const optional<string>& opt_src_geom)
{
	string res;

	res += GenTab(1) + info.name + "():ShaderBase(";

	res += "R\"---SRC---(";
	res += src_vert;
	res += ")---SRC---\"";

	res += ", ";

	res += "R\"---SRC---(";
	res += src_frag;
	res += ")---SRC---\"";

	if (opt_src_geom)
	{
		res += ", ";

		res += "R\"---SRC---(";
		res += opt_src_geom.value();
		res += ")---SRC---\"";
	}

	res += ") {}\n";
	return res;
}

string GenClassEnd()
{
	string res;

	res += "};";

	return res;
}


bool IsBasicVar(const Variable& var)
{
	return basic_type_map.find(var.type) != basic_type_map.end();
}

string BasicVarTypeToCppType(const Variable& var)
{
	return !var.array_size ?
		basic_type_map.at(var.type) :
		"std::vector<" + basic_type_map.at(var.type) + ">";
}

string VarTypeToCppType(const ProgramInfo& info, const Variable& var)
{
	if (IsBasicVar(var))
	{
		return BasicVarTypeToCppType(var);
	}

	return "ERROR";
}

string BasicVarTypeToGlSet(const Variable& var)
{
	string res;

	res += basic_type_to_call_map.at(var.type);
	if (!var.array_size)
	{
		res += "(loc, val);";
	}
	else
	{
		res += "(loc, val, size());";
	}

	return res;
}

string VarTypeToGlSet(const ProgramInfo& info, const Variable& var)
{
	if (IsBasicVar(var))
	{
		return BasicVarTypeToGlSet(var);
	}
	return "ERROR";
}

string GenPropertyBasic(const ProgramInfo& info, const Variable& var)
{
	ASSERT_MSG(IsBasicVar(var), "wrong call");
	string res;

	//class { friend class BASE; GLint loc; public: void operator = (const TTT& val) { GLCALL } } prop;

	res += "    class { friend struct ";
	res += info.name;
	res += "; GLint loc = -1; public: ";
	if (var.array_size)
	{
		res += "std::size_t size() { return " + to_string(var.array_size.value()) + "; } ";
	}
	res += "void operator = (const ";
	res += VarTypeToCppType(info, var);
	res += "& val) { ";
	res += VarTypeToGlSet(info, var);
	res += " } } ";
	res += var.name;
	res += ";\n";

	return res;
}

string GenStructProperty(const ProgramInfo& info, const Variable& var)
{
	ASSERT_MSG(!IsBasicVar(var), "wrong call");
	string res;

	res += "    ";
	if (!var.array_size)
	{
		res += var.type;
	}
	else
	{
		res += "std::array<" + var.type + ", " + std::to_string(var.array_size.value()) + ">";
	}
	res += " ";
	res += var.name;
	res += ";\n";

	return res;
}

string GenProperty(const ProgramInfo& info, const Variable& var)
{
	if (IsBasicVar(var))
	{
		return GenPropertyBasic(info, var);
	}
	else
	{
		return GenStructProperty(info, var);
	}
}

string GenPropertiesDefinitions(const ProgramInfo& info, const vector<Variable>& uniforms)
{
	string res;

	unordered_set<string> struct_names;
	for (auto& var : uniforms)
	{
		if (!IsBasicVar(var))
		{
			struct_names.insert(var.type);
		}
	}

	for (auto& struct_name : struct_names)
	{
		auto& str_info = info.structs.at(struct_name);

		res += GenTab(1) + "struct " + struct_name + "\n";
		res += GenTab(1) + "{\n";
		for (auto& field : str_info.fields)
		{
			ASSERT_MSG(IsBasicVar(field), "So complex:" + struct_name);
			res += GenTab(1) + GenPropertyBasic(info, field);
		}
		res += GenTab(1) + "};\n";
	}

	for (auto& var : uniforms)
	{
		res += GenProperty(info, var);
	}

	return res;
}

string GenBasicVarInit(const Variable& var)
{
	string res;

	res += var.name + ".loc";
	res += " = GetLocation(\"" + var.name + "\");";

	return res;
}

string GenStructVarInit(const ProgramInfo& info, const Variable& var)
{
	string res;

	auto& str_info = info.structs.at(var.type);

	if (!var.array_size)
	{
		for (auto& field : str_info.fields)
		{
			res += GenTab(2) + var.name + "." + field.name + ".loc";
			res += " = GetLocation(\"" + var.name + "." + field.name + "\");\n";
		}
	}
	else
	{
		res += GenTab(2) + "for (std::size_t i = 0; i < " + var.name + ".size(); i++)\n";
		res += GenTab(2) + "{\n";
		for (auto& field : str_info.fields)
		{

			res += GenTab(3) + var.name + "[i]." + field.name + ".loc";
			res += " = GetLocation(\"" + var.name + "[\" + std::to_string(i) + \"]." + field.name + "\");\n";
		}
		res += GenTab(2) + "}\n";
	}
	return res;
}

string GenInit(const ProgramInfo& info, const vector<Variable>& uniforms)
{
	string res;

	res += GenTab(1) + "void Init()\n";
	res += GenTab(1) + "{\n";
	res += GenTab(2) + "ShaderBase::Init();\n";

	for (auto& var : uniforms)
	{
		if (IsBasicVar(var))
		{
			res += GenTab(2) + GenBasicVarInit(var) + "\n";
		}
		else
		{
			res += GenStructVarInit(info, var);
		}
	}

	res += GenTab(1) + "}\n";

	return res;
}

string GenClassMembers(const ProgramInfo& info, const vector<Variable>& uniforms)
{
	string res;

	res += GenPropertiesDefinitions(info, uniforms);
	res += GenInit(info, uniforms);

	return res;
}

string Generate(const ProgramInfo& info, const string& src_vert, const string& src_frag, const optional<string>& opt_src_geom)
{
	vector<Variable> uniforms;
	for (auto& pair : info.uniforms)
	{
		uniforms.push_back(pair.second);
	}
	sort(uniforms.begin(), uniforms.end());

	return
		GenBeginFile() +
		GenClassBegin(info) +
		GenClassConstructor(info, src_vert, src_frag, opt_src_geom) +
		GenClassMembers(info, uniforms) +
		GenClassEnd();
}


