#pragma once
#include "pch.h"
#include "program_info.h"

string Generate(const ProgramInfo& info, const string& src_vert, const string& src_frag, const optional<string>& opt_src_geom);