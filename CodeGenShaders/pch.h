#pragma once

#include <array>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <string>
#include <iostream>
#include <fstream>
#include <optional>
#include <algorithm>
#include <filesystem>
#include <numeric>

using namespace std;

#define ASSERT(EX)          AssertImpl((bool)(EX), #EX, __FILE__, __LINE__)
#define ASSERT_MSG(EX, msg) AssertMsgImpl((bool)(EX), #EX, msg, __FILE__, __LINE__)

inline void AssertMsgImpl(bool is_ex, const string& ex, const string& msg, const string& file, size_t line)
{
	if (!is_ex)
	{
		cout << "Assert" << endl;
		cout << "Condition:" << ex << endl;
		cout << "Message:" << msg << endl;
		cout << "File:" << file << endl;
		cout << "Line:" << line << endl;
		terminate();
	}
}

inline void AssertImpl(bool is_ex, const string& ex, const string& file, size_t line)
{
	if (!is_ex)
	{
		cout << "Assert" << endl;
		cout << "Condition:" << ex << endl;
		cout << "File:" << file << endl;
		cout << "Line:" << line << endl;
		terminate();
	}
}