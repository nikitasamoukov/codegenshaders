#pragma once
#include "pch.h"

struct Variable
{
	string name;
	string type;
	optional<size_t> array_size;
	int group = 0;

	bool IsArray() { return bool(array_size); }

	friend ostream& operator<<(ostream& os, const Variable& var);

	friend bool operator<(const Variable& a, const Variable& b)
	{
		if (a.group < b.group)
			return true;
		if (a.group > b.group)
			return false;
		return a.name < b.name;
	}
};

struct StructInfo
{
	vector<Variable> fields;
};

struct ProgramInfo
{
	string name;

	unordered_map<string, StructInfo> structs;
	unordered_map<string, Variable> uniforms;
	unordered_map<string, string> defines;

	friend ostream& operator<<(ostream& os, const ProgramInfo& info);
};

ProgramInfo Merge(const ProgramInfo& a, const ProgramInfo& b);
