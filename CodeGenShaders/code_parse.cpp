#include "pch.h"
#include "code_parse.h"
#include "program_info.h"

using Expression = vector<string>;
using Expressions = vector<vector<string>>;
using Lex = vector<string>;

string RemoveComments(const string& text)
{
	string res;

	size_t pos = 0;
	auto pos_one_comment = text.find("//");
	auto pos_multi_comment = text.find("/*");
	while (true)
	{
		if (pos_one_comment < pos)
		{
			pos_one_comment = text.find("//", pos);
		}
		if (pos_multi_comment < pos)
		{
			pos_multi_comment = text.find("/*", pos);
		}
		if (pos_one_comment < pos_multi_comment)
		{
			res += text.substr(pos, pos_one_comment - pos);
			pos = text.find('\n', pos_one_comment);
			if (pos != string::npos && text[pos - 1] == '\r')
			{
				res += '\r';
			}
		}
		if (pos_multi_comment < pos_one_comment)
		{
			res += text.substr(pos, pos_multi_comment - pos);
			pos = text.find("*/", pos_multi_comment + 2) + 2;
		}
		if (pos == string::npos)
		{
			return res;
		}
		if (pos_one_comment == pos_multi_comment)
		{
			res += text.substr(pos, text.size() - pos);
			return res;
		}
	}
}

enum class CHAR_TYPE :uint8_t
{
	EMPTY,
	LEX_CHAR,
	TEXT
};

array<CHAR_TYPE, 256> CreateArrayMap()
{
	array<CHAR_TYPE, 256> res{};
	fill(res.begin(), res.end(), CHAR_TYPE::TEXT);
	for (char ch : string(" \t\r"))
	{
		res[uint8_t(ch)] = CHAR_TYPE::EMPTY;
	}
	for (char ch : string("{}[];\n"))
	{
		res[uint8_t(ch)] = CHAR_TYPE::LEX_CHAR;
	}
	return res;
}
array<CHAR_TYPE, 256> type_of_char = CreateArrayMap();

size_t FindNot(const string& text, size_t offset, CHAR_TYPE type)
{
	for (size_t i = offset; i < text.size(); i++)
	{
		if (type_of_char[text[i]] != type)
		{
			return i;
		}
	}

	return string::npos;
}

Lex ToLex(const string& code)
{
	if (code.empty())
	{
		return{};
	}

	Lex res;
	size_t pos = 0;

	while (true)
	{
		CHAR_TYPE t = type_of_char[uint8_t(code[pos])];
		size_t pos_end = FindNot(code, pos, t);
		switch (t)
		{
		case CHAR_TYPE::EMPTY:
			if (pos_end == string::npos)
			{
				res.emplace_back("\n");
				res.emplace_back(";");
				return res;
			}
			break;
		case CHAR_TYPE::LEX_CHAR:
			if (pos_end == string::npos)
			{
				for (size_t i = pos; i < code.size(); i++)
				{
					res.emplace_back(string(1, code[i]));
				}
				res.emplace_back("\n");
				res.emplace_back(";");
				return res;
			}

			for (size_t i = pos; i < pos_end; i++)
			{
				res.emplace_back(string(1, code[i]));
			}
			break;
		case CHAR_TYPE::TEXT:
			if (pos_end == string::npos)
			{
				res.emplace_back(code.substr(pos, code.size() - pos));

				res.emplace_back("\n");
				res.emplace_back(";");
				return res;
			}

			res.emplace_back(code.substr(pos, pos_end - pos));
			break;
		}
		pos = pos_end;
	}
}

Expressions LexToExpressions(const Lex& lex)
{
	Expressions res;

	for (size_t pos = 0; pos < lex.size();)
	{
		if (lex[pos][0] == '#')
		{
			Expression tmp;
			while (lex[pos][0] != '\n')
			{
				tmp.emplace_back(lex[pos]);
				pos++;
			}
			res.emplace_back(move(tmp));
		}
		else
		{
			Expression tmp;
			while (lex[pos][0] != ';')
			{
				if (lex[pos][0] == '{' || lex[pos][0] == '}')
				{
					if (!tmp.empty())
					{
						res.emplace_back(move(tmp));
					}
					tmp.clear();

					res.emplace_back(Expression({ lex[pos] }));
				}
				else
				{
					if (lex[pos][0] != '\n')
					{
						tmp.emplace_back(lex[pos]);
					}
					else
					{
					}
				}
				pos++;
				if (tmp.empty())
				{
					break;
				}
			}
			if (lex[pos][0] == ';')
			{
				pos++;
			}
			if (!tmp.empty())
			{
				res.emplace_back(move(tmp));
			}
		}
	}

	return res;
}

Variable ExpressionToVar(const ProgramInfo& info, const Expression& ex)
{
	Variable res;
	ASSERT_MSG(ex.size() == 2 || ex.size() == 5, "Wrong uniform variable parse");

	if (ex.size() == 2)
	{
		res.name = ex[1];
		res.type = ex[0];
	}

	if (ex.size() == 5)
	{
		res.name = ex[1];
		res.type = ex[0];
		if (isdigit(ex[3][0]))
		{
			res.array_size = stoull(ex[3]);
		}
		else
		{
			ASSERT_MSG(info.defines.find(ex[3]) != info.defines.end(), "only simple '#define N 10' or [10] supported");
			res.array_size = stoull(info.defines.at(ex[3]));
		}
	}
	return res;
}

Variable ExpressionUniformToVar(const ProgramInfo& info, const Expression& ex)
{
	Variable res;
	ASSERT_MSG(ex.size() == 3 || ex.size() == 6, "Wrong uniform variable parse");
	ASSERT_MSG(ex[0] == "uniform", "Wrong uniform variable parse");

	if (ex.size() == 3)
	{
		res.name = ex[2];
		res.type = ex[1];
	}

	if (ex.size() == 6)
	{
		res.name = ex[2];
		res.type = ex[1];
		if (isdigit(ex[4][0]))
		{
			res.array_size = stoull(ex[4]);
		}
		else
		{
			ASSERT_MSG(info.defines.find(ex[4]) != info.defines.end(), "only simple '#define N 10' or [10] supported");
			res.array_size = stoull(info.defines.at(ex[4]));
		}
	}
	return res;
}

ProgramInfo ExpressionsToProgramInfo(const Expressions& ex_arr)
{
	ProgramInfo res;

	for (size_t i = 0; i < ex_arr.size();)
	{
		if (ex_arr[i][0] == "#define" && ex_arr[i].size() == 3)
		{
			res.defines.insert({ ex_arr[i][1],ex_arr[i][2] });
		}
		if (ex_arr[i][0] == "struct")
		{
			ASSERT_MSG(ex_arr[i].size() == 2, "wrong parse struct");
			string struct_name = ex_arr[i][1];
			StructInfo struct_info;
			ASSERT_MSG(i + 1 < ex_arr.size(), "wrong parse struct");
			ASSERT_MSG(ex_arr[i + 1][0] == "{", "wrong parse struct");
			i += 2;
			while (ex_arr[i][0] != "}")
			{
				Variable var = ExpressionToVar(res, ex_arr[i]);
				struct_info.fields.emplace_back(move(var));
				i++;
				ASSERT_MSG(i < ex_arr.size(), "wrong parse struct");
			}
			res.structs.insert({ struct_name, struct_info });
		}
		if (ex_arr[i][0] == "uniform")
		{
			Variable var = ExpressionUniformToVar(res, ex_arr[i]);
			res.uniforms.insert({ var.name,var });
		}
		i++;
	}

	return res;
}

ProgramInfo GetProgramInfoFromCode(const string& program_src)
{
	string program_cleaned = RemoveComments(program_src);
	auto program_lex = ToLex(program_cleaned);

	//for (const auto& word : program_lex)
	//{
	//	cout << " " << word;
	//}

	auto program_ex = LexToExpressions(program_lex);

	//cout << "---------------------------------------------------------------------------------------------------------------" << endl;
	//for (const auto& line : program_ex)
	//{
	//	for (const auto& word : line)
	//	{
	//		cout << word << " ";
	//	}
	//	cout << endl;
	//}

	auto program_pi = ExpressionsToProgramInfo(program_ex);

	//cout << program_pi << endl;

	return program_pi;
}

ProgramInfo Parse(const string& vert_src, const string& frag_src, const string& geom_src)
{
	vector<ProgramInfo> infos;
	infos.push_back(GetProgramInfoFromCode(vert_src));

	ProgramInfo frag_info = GetProgramInfoFromCode(frag_src);
	for_each(frag_info.uniforms.begin(), frag_info.uniforms.end(), [](auto& var) {var.second.group = 1; });
	infos.push_back(frag_info);

	if (!geom_src.empty())
	{
		ProgramInfo geom_info = GetProgramInfoFromCode(vert_src);
		for_each(geom_info.uniforms.begin(), geom_info.uniforms.end(), [](auto& var) {var.second.group = 2; });
		infos.push_back(geom_info);
	}

	ProgramInfo full_info = accumulate(infos.begin(), infos.end(), ProgramInfo(), Merge);

	return full_info;
}
