#include "pch.h"
#include "program_info.h"

ostream& operator<<(ostream& os, const Variable& var)
{
	if (var.array_size)
	{
		os << var.type << " " << var.name << "[" << var.array_size.value() << "]";
	}
	else
	{
		os << var.type << " " << var.name;
	}
	return os;
}

ostream& operator<<(ostream& os, const ProgramInfo& info)
{
	os << "Structs:" << endl;
	for (const auto& pair : info.structs)
	{
		auto& fields = pair.second.fields;
		os << pair.first << endl;
		os << "{" << endl;
		for (auto& field : fields)
		{
			os << "    " << field << endl;
		}
		os << "}" << endl;
	}

	os << "Uniforms:" << endl;
	for (auto& pair : info.uniforms)
	{
		os << pair.second << endl;
	}
	return os;
}

ProgramInfo Merge(const ProgramInfo& a, const ProgramInfo& b)
{
	ProgramInfo info = a;
	for (const auto& pair : b.structs)
	{
		ASSERT_MSG(info.structs.find(pair.first) == info.structs.end(), "same struct definitions in different files");
		info.structs.insert(pair);
	}
	for (const auto& pair : b.uniforms)
	{
		if (info.uniforms.find(pair.first) != info.uniforms.end())
		{
			ASSERT_MSG(
				pair.second.name == info.uniforms[pair.first].name &&
				pair.second.array_size == info.uniforms[pair.first].array_size,
				"same uniform name, but different definitions");
		}
		info.uniforms.insert(pair);
	}
	return info;
}