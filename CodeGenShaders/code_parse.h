#pragma once
#include "pch.h"
#include "program_info.h"

ProgramInfo Parse(const string& vert_src, const string& frag_src, const string& geom_src = "");